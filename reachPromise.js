/*
Given the following code:



let promise1 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 500, 'promise1');
});

let promise2 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 1000, 'promise2');
});

let promise3 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 1200, 'promise3');
});

let promise4 = new Promise(function(resolve, reject) {
    setTimeout(reject, 300, 'promise4');
});

let promise5 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 800, 'promise5');
});

let promise = Promise.all([promise1, promise2, promise3, promise4, promise5]);

promise

.then(function(data) {
    data.forEach(function(data) {
        console.log(data);
    });
})

.catch(function(error) {
    console.error('error', error);
});


The Problem: We can't access the result of the other promises.

If a given promise is rejected, the resulting promise of 'Promise.all' will be 
rejected at this exact moment. It will not wait for the other promises to complete, 
and the only received data is the error of the rejected request.

So, error is displayed:

```
error promise4

```

What if you want to start multiple asynchronous jobs at once and you want results 
even if a job is rejected?


//+++ Constrainsts

- Define 'getResult()' function that returns a promise and resolve an array with
the results of multiple asynchronous jobs.

What to do for reaching the following output:

```
[
    'promise1',
    'promise2',
    'promise3',
    Error: Promise 4
    at C:\...
    at async Promise.all ...,
    'promise5'
]
```

Tests must be true.

*/


//+++ YOUR CODE GOES HERE



//promises








//getResult()









// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*



getResult()

.then(data => {
    console.log("Test 1...",  data.indexOf('promise1') >= 0)
    console.log("Test 2...",  data.indexOf('promise2') >= 0)
    console.log("Test 3...",  data.indexOf('promise3') >= 0)
    console.log("Test 4...",  data[3].message === 'promise4')
    console.log("Test 5...",  data[4] === 'promise5')
})

.catch((e) => console.log(e))

















/* Source:

- Adapted from https://www.codingame.com/playgrounds/347/javascript-promises-mastering-the-asynchronous/traps-of-promises

*/